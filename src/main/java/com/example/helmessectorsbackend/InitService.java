package com.example.helmessectorsbackend;

import com.example.helmessectorsbackend.entity.Sector;
import com.example.helmessectorsbackend.service.SectorService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class InitService implements InitializingBean {

    @Autowired
    private SectorService sectorService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Path path = Paths.get(getClass().getClassLoader()
                .getResource("static/index.html").toURI());

//        Stream<String> lines = Files.lines(path, StandardCharsets.ISO_8859_1);
        Stream<String> lines = Files.lines(path, Charset.forName("windows-1252"));
        String html = lines.collect(Collectors.joining("\n"));
        lines.close();

        List<Sector> sectors = new ArrayList<>();
        int prevLevel = 0;
        Sector latest = null;

        Document doc = Jsoup.parse(html);
        Elements options = doc.select("option");
        for(Element el : options) {
            int count = el.wholeText().indexOf(el.text());
            int level = computeLevel(4, count);

            if (level == 0) {
                latest = new Sector(Integer.valueOf(el.val()), el.text(), el.wholeText());
                sectors.add(latest);
                prevLevel = level;
                continue;
            }
            int diff = level - prevLevel;
            Sector parent = latest;
            for (int i = 0; i >= diff; i--) {
                parent = parent.getParent();
            }
            Sector sector = new Sector(Integer.valueOf(el.val()), el.text(), el.wholeText(), parent);
            sectors.add(sector);
            latest = sector;

            prevLevel = level;
        }
        sectorService.saveAll(sectors);
    }

    private int computeLevel(int step, int value) {
        if (value == 0) return 0;
        return value/step;
    }
}