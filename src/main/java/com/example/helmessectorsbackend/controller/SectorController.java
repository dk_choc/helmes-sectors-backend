package com.example.helmessectorsbackend.controller;

import com.example.helmessectorsbackend.entity.Sector;
import com.example.helmessectorsbackend.service.SectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/sector")
public class SectorController {

    @Autowired
    private SectorService sectorService;

    /**
     * Get all available sectors in sorted order.
     * @return List of sectors.
     */
    @GetMapping("/all")
    public Set<Sector> getSectors(){
        return sectorService.getAllSectors();
    }
}
