package com.example.helmessectorsbackend.controller;

import com.example.helmessectorsbackend.util.Messages;
import com.example.helmessectorsbackend.dto.UserDto;
import com.example.helmessectorsbackend.entity.User;
import com.example.helmessectorsbackend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user-sector")
public class UserSectorController {
    private static final Logger log = LoggerFactory.getLogger(UserSectorController.class);

    @Autowired
    private UserService userService;

    /**
     * Get user by id.
     * @param id User id.
     * @return User.
     */
    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable(value = "id") String id) {
        log.info(Messages.USER_REQUEST, id);
        return userService.convertToDto(userService.getById(Long.valueOf(id)));
    }

    /**
     * Save new user to database.
     * @param user User to be saved.
     * @return Saved user.
     */
    @PostMapping
    public UserDto saveUser(@RequestBody UserDto user){
        log.info(Messages.USER_SAVE_REQUEST, user.toString());
        User save = userService.save(userService.convertToEntity(user));
        if (save != null) {
            log.info(Messages.USER_SAVE_SUCCESS);
            return userService.convertToDto(save);
        } else {
            log.warn(Messages.USER_SAVE_FAILED);
        }
        return user;
    }

    /**
     * Update existing user in database.
     * @param user User to be updated.
     * @return Updated user.
     */
    @PutMapping
    public UserDto updateUser(@RequestBody UserDto user) {
        log.info(Messages.USER_UPDATE_REQUEST, user.toString());

        User update = userService.update(userService.convertToEntity(user));
        if (update != null) {
            log.info(Messages.USER_UPDATE_SUCCESS);
            return userService.convertToDto(update);
        } else {
            log.warn(Messages.USER_UPDATE_FAILED);
        }
        return user;
    }
}
