package com.example.helmessectorsbackend.dto;

import java.util.LinkedHashSet;
import java.util.Set;

public class UserDto {

    private Long id;
    private String name;
    private Set<Integer> sectors = new LinkedHashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Integer> getSectors() {
        return sectors;
    }

    public void setSectors(Set<Integer> sectors) {
        this.sectors = sectors;
    }

    public void addSector(Integer sector) {
        this.sectors.add(sector);
    }

    public void removeSector(Integer sector) {
        this.sectors.remove(sector);
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sectors=" + sectors +
                '}';
    }
}
