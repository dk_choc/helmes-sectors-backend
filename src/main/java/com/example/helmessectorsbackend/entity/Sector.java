package com.example.helmessectorsbackend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "SECTOR")
public class Sector {

    @Id
    private Integer id;

    @Column(name = "name", length = 250, nullable = false)
    private String name;

    @Column(name = "label", length = 250, nullable = false)
    private String label;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="parent_id")
    private Sector parent;

    @JsonIgnore
    @OneToMany(mappedBy="parent", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Sector> children = new LinkedHashSet<>();

    public Sector() { }

    public Sector(Integer id, String name, String label) {
        this.id = id;
        this.name = name;
        this.label = label;
    }

    public Sector(Integer id, String name, String label, Sector parent) {
        this.id = id;
        this.name = name;
        this.label = label;
        this.parent = parent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Sector getParent() {
        return parent;
    }

    public void setParent(Sector parent) {
        this.parent = parent;
    }

    public Set<Sector> getChildren() {
        return children;
    }

    public void setChildren(Set<Sector> children) {
        this.children = children;
    }
}
