package com.example.helmessectorsbackend.repository;

import com.example.helmessectorsbackend.entity.Sector;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface SectorRepository extends JpaRepository<Sector, Integer> {

    Set<Sector> findByParentIsNull();
}
