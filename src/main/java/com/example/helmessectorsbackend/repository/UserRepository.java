package com.example.helmessectorsbackend.repository;

import com.example.helmessectorsbackend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
