package com.example.helmessectorsbackend.service;

import com.example.helmessectorsbackend.entity.Sector;
import com.example.helmessectorsbackend.repository.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
public class SectorService {

    @Autowired
    private SectorRepository sectorRepository;

    public List<Sector> saveAll(List<Sector> sectors) {
        return sectorRepository.saveAll(sectors);
    }

    public Set<Sector> getParentSectors(){
        return sectorRepository.findByParentIsNull();
    }

    public Set<Sector> getAllSectors(){
        return getChildren(new LinkedHashSet<>(), getParentSectors());
    }

    private Set<Sector> getChildren(Set<Sector> visitedSectors, Set<Sector> parentSectors){
        for (Sector sector : parentSectors) {
            visitedSectors.add(sector);
            getChildren(visitedSectors, (sector.getChildren()));
        }
        return visitedSectors;
    }
}
