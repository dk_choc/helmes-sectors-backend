package com.example.helmessectorsbackend.service;

import com.example.helmessectorsbackend.dto.UserDto;
import com.example.helmessectorsbackend.entity.Sector;
import com.example.helmessectorsbackend.entity.User;
import com.example.helmessectorsbackend.repository.SectorRepository;
import com.example.helmessectorsbackend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SectorRepository sectorRepository;

    public User getById(Long id){
        return userRepository.findById(id).orElse(null);
    }

    public User update(User user){
        User existingUser = userRepository.findById(user.getId()).orElse(null);
        if (existingUser != null){
            existingUser.setName(user.getName());
            existingUser.setSectors(user.getSectors());
            save(existingUser);
        }
        return existingUser;
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public User convertToEntity(UserDto userDto){
        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        Set<Sector> sectors = new LinkedHashSet<>();
        for(Integer sectorId : userDto.getSectors()){
            Sector sector = sectorRepository.findById(sectorId).orElse(null);
            if (sector != null)
                sectors.add(sector);
        }
        user.setSectors(sectors);
        return user;
    }

    public UserDto convertToDto(User user){
        if (user == null)
            return null;
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        Set<Integer> sectors = new HashSet<>();
        for(Sector sector : user.getSectors()){
            sectors.add(sector.getId());
        }
        userDto.setSectors(sectors);
        return userDto;
    }
}
