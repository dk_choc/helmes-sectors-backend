package com.example.helmessectorsbackend.util;

public class Messages {

    public static final String USER_REQUEST = "Getting user by id {}";
    public static final String USER_SAVE_REQUEST = "Saving new user {}";
    public static final String USER_UPDATE_REQUEST = "Updating existing user {}";

    public static final String USER_SAVE_SUCCESS = "User saved successfully";
    public static final String USER_SAVE_FAILED = "User saving failed";
    public static final String USER_UPDATE_SUCCESS = "User updates successfully";
    public static final String USER_UPDATE_FAILED = "User updating failed";


}
